<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/*  Use the Room, Meeting, Auth and User namespaces so we can use them to pass data to the view */
use App\Room;
use App\User;
use App\Meeting;

/*  Use the Auth namespace so we can access user information */
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /*  Get rooms from the database to display on the form  */
        $rooms = Room::all();

        /*  Get all the users so we can filter meetings */
        $users = User::all();

        /*  Get all the meetings for this user so we can show a list to edit them  */
        $meetings = Meeting::where('organizer_id', '=', Auth::id())->get();

        foreach ($meetings as $meeting)
        {
            /*  Convert the int dates to a readable format  */
            $meeting->start_datetime = date('m/d/Y g:i A', $meeting->start_datetime);
            $meeting->end_datetime = date('m/d/Y g:i A', $meeting->end_datetime);
        }
        
        /*  Pass the data on to the view  */
        return view('home', compact('rooms', 'users', 'meetings'));
    }
}
