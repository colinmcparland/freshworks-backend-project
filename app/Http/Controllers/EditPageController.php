<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/*  Use the Room, Meeting, Auth and User namespaces so we can use them to pass data to the view */
use App\Room;
use App\User;
use App\Meeting;


class EditPageController extends Controller
{

	 /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Show the edit meeting screen.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
    	/*  If we have an empty ID, go back to the previous page.  */
    	if(empty($id))
    	{
    		return redirect()->back();
    	}

        /*  Get rooms from the database to display on the form  */
        $rooms = Room::all();

        /*  Get all the users so we can filter meetings */
        $users = User::all();

        /*  Get all the meetings for this user so we can show a list to edit them  */
        $meeting = Meeting::find($id);


        /*  Convert the int dates to a readable format  */
        $meeting->start_datetime = date('m/d/Y g:i A', $meeting->start_datetime);
        $meeting->end_datetime = date('m/d/Y g:i A', $meeting->end_datetime);

        
        /*  Pass the data on to the view  */
        return view('edit-meeting', compact('rooms', 'users', 'meeting'));
    }
}
