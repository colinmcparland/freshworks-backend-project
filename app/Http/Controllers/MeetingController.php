<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/* Use our models namespace so we can get Eloquent with it */
use \App\Meeting;

/*  Use the Auth namespace so we can access user information */
use Illuminate\Support\Facades\Auth;

class MeetingController extends Controller
{

    /*  
        Function to create a new meeting.  Redirects with error message on fail and returns JSON dump of new meeting on success.
    */

    public function create() {

        /*  Declare variables  */
        $temp_start;
        $temp_end;
        $this_start = strtotime(request('startDateTime'));
        $this_end = strtotime(request('endDateTime'));
        $room = request('roomSelect');
        $user_id = Auth::user()->id;

    	/*  Check if this room is booked at the desired time.  Use some variables to make it easier to read */
        $temp_meetings = Meeting::where('room_id', '=', $room);

        foreach($temp_meetings as $temp_meeting)
        {
            /*  These are already strtotime'd so were good to compare  */
            $temp_start = $temp_meeting->start_datetime;
            $temp_end = $temp_meeting->end_datetime;

            if($temp_start > $this_end  || $temp_end < $this_start)  //  We good
            {

            }
            else  //  Theres a conflict
            {
                return redirect()->back()->with('new_meeting_error', 'There is already a meeting in that time slot.  Please select another time.');
            } 
        }

    	/*  Book the room */
    	$meeting = new Meeting;
    	$meeting->start_datetime = $this_start;
    	$meeting->end_datetime = $this_end;
    	$meeting->organizer_id = $user_id;
    	$meeting->room_id = $room;
    	$meeting->save();

        /*  Return the new meeting as JSON */
    	return response()->json(Meeting::find($meeting->id));
    }


    /*  
        Function to create a new meeting.  Redirects with error message on fail and returns JSON dump of new meeting on success.
    */

    public function api_create() {

        /*  Declare variables  */
        $temp_start;
        $temp_end;
        $this_start = strtotime(request('startDateTime'));
        $this_end = strtotime(request('endDateTime'));
        $room = request('roomSelect');
        $user_id = Auth::user()->id;

        /*  Check if this room is booked at the desired time.  Use some variables to make it easier to read */
        $temp_meetings = Meeting::where('room_id', '=', $room);

        foreach($temp_meetings as $temp_meeting)
        {
            /*  These are already strtotime'd so were good to compare  */
            $temp_start = $temp_meeting->start_datetime;
            $temp_end = $temp_meeting->end_datetime;

            if($temp_start > $this_end  || $temp_end < $this_start)  //  We good
            {

            }
            else  //  Theres a conflict
            {
                return redirect()->back()->with('new_meeting_error', 'There is already a meeting in that time slot.  Please select another time.');
            } 
        }

        /*  Book the room */
        $meeting = new Meeting;
        $meeting->start_datetime = $this_start;
        $meeting->end_datetime = $this_end;
        $meeting->organizer_id = $user_id;
        $meeting->room_id = $room;
        $meeting->save();

        /*  Return the new meeting as JSON */
        return response()->json(Meeting::find($meeting->id));
    }



    /*
        Function to retreive a list of meetings in JSON format
    */

    public function retreive() {

        /*  Get all the arguments so we can get the right meetings  */
        $start = request('searchStartDateTime');
        $end = request('searchEndDateTime');
        $room = request('roomSelect');
        $user = request('userSelect');

        $meetings = new Meeting;

        /*  Build the query  */
        if(!empty($start))
        {
            $meetings = $meetings->where('start_datetime', '>=', strtotime($start))->where('end_datetime', '<=', strtotime($end));
        }

        if(!empty($room))
        {
            $meetings = $meetings->where('room_id', '=', $room);
        }

        if(!empty($user)) {
            $meetings = $meetings->where('organizer_id', '=', $user);
        }

        /*  Return a JSON string  */
        return response()->json($meetings->get());
    }




    /*
        Function to edit a meeting, returns redirect to the dashboard
    */

    public function edit($id) {

        /*  If we have an empty ID, go back to the previous page.  */
        if(empty($id))
        {
            return redirect()->back();
        }

        /*  Declare variables  */
        $temp_start;
        $temp_end;
        $this_start = strtotime(request('editStartDateTime'));
        $this_end = strtotime(request('editEndDateTime'));
        $room = request('editRoomSelect');
        $user_id = Auth::user()->id;

        /*  Check if this room is booked at the desired time.  Use some variables to make it easier to read */
        $temp_meetings = Meeting::where('room_id', '=', $room);

        foreach($temp_meetings as $temp_meeting)
        {
            /*  These are already strtotime'd so were good to compare  */
            $temp_start = $temp_meeting->start_datetime;
            $temp_end = $temp_meeting->end_datetime;

            if($temp_start > $this_end  || $temp_end < $this_start)  //  We good
            {

            }
            else  //  Theres a conflict
            {
                return redirect()->back()->with('edit_meeting_error', 'There is already a meeting in that time slot.  Please select another time.');
            } 
        }

        /*  Book the room */
        $meeting = Meeting::find($id);
        $meeting->start_datetime = $this_start;
        $meeting->end_datetime = $this_end;
        $meeting->organizer_id = $user_id;
        $meeting->room_id = $room;
        $meeting->save();

        /*  Return the new meeting as JSON */
        return response()->json(Meeting::find($meeting->id));
    }


    /*
        Function to delete a meeting.
    */
    public function delete($id) {
        $meeting = Meeting::find($id);
        $meeting->delete();

        return redirect()->route('home');
    }

}
