<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeetingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('meetings', function (Blueprint $table) {
               $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('start_datetime');
            $table->string('end_datetime');
            $table->integer('organizer_id')->unsigned();
            $table->integer('room_id')->unsigned();
            $table->timestamps();
            $table->foreign('organizer_id')->references('id')->on('users');
            $table->foreign('room_id')->references('id')->on('rooms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('meetings');
    }
}
