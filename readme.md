# Freshworks Back End Project
http://18.220.191.108/
---
This project is a meeting room scheduler written in PHP using the Laravel framework.  Some more details about the project:

* Deployed on Amazon EC2 Linux instance 
* Uses the LAMP Stack
* Uses mysql database server
* Using PHP 7.0
* Using Eloquent ORM as by database abstraction layer since it comes with Laravel
* Built with a front-end to illustrate features, can be made more command line accessible and secure using Laravel Passport.  However only included front-end authentication with username and password, so not accessible with oAuth flow on curl, postman, etc..  However, have added unprotected GET and PSOT endpoints for illustrative purposes 


##Description
---
* There are a total of 3 meeting rooms, that have been manually entered into the database.
* POST and Response Bodies are JSON encoded  
* Email and Password Authentication to login to front-end to make API requests.  No command line support at this time as didn't have time to delve into Laravel Passport (TODO!!)  
* Authenticated users can book a meeting into a selected room, given  the date and time.  
* Users can not schedule a meeting in an overlapping time slot  
* Users can cancel and/or edit meetings that they created.  
* Users can retrieve a list of rooms.  
* Users can retrieve a list of meetings in a given room, filtered by a date range  
* Users can retrieve a list of all meetings for a given user, filtered by a date  range.  
* The Database Model is provided in a seperate sql file.

## Sample cURL requests
---
`curl -H 'content-type: application/json' http://18.220.191.108/api/meeting`
`curl -H 'content-type: application/json' http://18.220.191.108/api/meeting?roomSelect=3`  
`curl -d '{"roomSelect":"1", "startDateTime":"32503720500", "endDateTime":"32503820500"}' -H "Content-Type: application/json" -X POST http://18.220.191.108/api/meeting/create`

