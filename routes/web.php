<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*  Load the Auth routes  */
Auth::routes();

/*  Home route  */
Route::get('/', 'HomeController@index')->name('home');

/*  Edit meeting route  */
Route::get('/edit/{id}', 'EditPageController@index');

/*
	When we POST to the create endpoint from the front end, call the create function in the MeetingController
*/
Route::post('/meeting/create', 'MeetingController@create');


/*
	When we POST to the edit endpoint, call the edit function in the MeetingController
*/
Route::post('/meeting/edit/{id}', 'MeetingController@edit');

/*
	When we GET a meeting with arguments, return a json string in the Meeting controller
*/
Route::get('/meeting', 'MeetingController@retreive')->middleware('auth');

/*
	When we DELETE a meeting, call the appropriate function
*/
Route::delete('/meeting/delete/{id}', 'MeetingController@delete');
