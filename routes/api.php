<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

/*  Start unprotected API endpoints  TODO:  Add Passport oAuth authentication  */


/*
	When we GET a meeting with arguments, return a json string in the Meeting controller
*/
Route::get('/meeting', 'MeetingController@retreive');

/*
	When we POST to the edit endpoint, call the edit function in the MeetingController
*/
Route::post('/meeting/edit/{id}', 'MeetingController@edit');

/*
	When we DELETE a meeting, call the appropriate function
*/
Route::delete('/meeting/delete/{id}', 'MeetingController@delete');

/*
	When we POST to the create endpoint from the front end, call the create function in the MeetingController
*/
Route::post('/meeting/create', 'MeetingController@api_create');