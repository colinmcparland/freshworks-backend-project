let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.scripts([
		'resources/assets/js/moment-with-locales.min.js',
		'resources/assets/js/bootstrap-datetimepicker.min.js'
		],
	'public/js/all.js')
   .sass('resources/assets/sass/app.scss', 'public/css')
   .styles([
   		'resources/assets/sass/bootstrap-datetimepicker.min.css'
   	], 'public/css/timepicker.css');
