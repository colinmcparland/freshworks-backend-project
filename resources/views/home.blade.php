@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Book a Meeting</div>
                <div class="panel-body">
                @include('partials.new-meeting-form')
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Search Meetings</div>
                <div class="panel-body">
                @include('partials.search-meetings-form')
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">Edit My Meetings</div>
                <div class="panel-body">
                @include('partials.edit-meetings-list')
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
