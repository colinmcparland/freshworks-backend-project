{{-- Snippet for the new meeting form --}}
<form action="/meeting/create" method="POST" class="new-meeting-form">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="roomSelect">Room ID</label>
        <select name="roomSelect" id="" class="form-control">
            @foreach($rooms as $room)
            <option value="{{ $room->id }}">{{ $room->id }}</option>
            @endforeach;
        </select>
    </div>
    <label for="startDateTime">Start Date and Time</label>
    <div class="form-group input-group date" id="startdatetimepicker">
        <input type="text" class="form-control" name="startDateTime">
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
    <label for="endDateTime">End Date and Time</label>
    <div class="form-group input-group date" id="enddatetimepicker">
        <input type="text" class="form-control" name="endDateTime">
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>

@if(session('new_meeting_error'))
<div class="alert alert-danger" role="alert">
   {{ session('new_meeting_error') }}
</div>
@endif
