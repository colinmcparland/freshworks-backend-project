{{-- Snippet for the meeting search form --}}

<form action="/meeting" method="GET" class="search-meeting-form">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="roomSelect">Room ID <super><small>Optional</small></super></label>
        <select name="roomSelect" id="" class="form-control">
            <option></option>
            @foreach($rooms as $room)
            <option value="{{ $room->id }}">{{ $room->id }}</option>
            @endforeach;
        </select>
    </div>
    <div class="form-group">
        <label for="roomSelect">User <super><small>Optional</small></super></label>
        <select name="userSelect" id="" class="form-control">
            <option></option>
            @foreach($users as $user)
            <option value="{{ $user->id }}">{{ $user->name }}</option>
            @endforeach;
        </select>
    </div>
    <label for="searchStartDateTime">Start Date and Time <super><small>Optional</small></super></label>
    <div class="form-group input-group date" id="searchstartdatetimepicker">
        <input type="text" class="form-control" name="searchStartDateTime">
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
    <label for="searchEndDateTime">End Date and Time <super><small>Optional</small></super></label>
    <div class="form-group input-group date" id="searchenddatetimepicker">
        <input type="text" class="form-control" name="searchEndDateTime">
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>

