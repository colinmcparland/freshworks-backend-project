{{-- Snippet for the new meeting form --}}
<form action="/meeting/edit/{{ $meeting->id }}" method="POST" class="edit-meeting-form">
    {{ csrf_field() }}
    <div class="form-group">
        <label for="editRoomSelect">Room ID</label>
        <select name="editRoomSelect" id="" class="form-control">
        {{-- Make the correct room selected --}}
            @foreach($rooms as $room)
                @if($room->id == $meeting->room_id)
                    <option selected value="{{ $room->id }}">{{ $room->id }}</option>
                @else
                    <option value="{{ $room->id }}">{{ $room->id }}</option>
                @endif
            @endforeach;
        </select>
    </div>
    <label for="startDateTime">Start Date and Time</label>
    <div class="form-group input-group date" id="editstartdatetimepicker">
        <input type="text" class="form-control" name="editStartDateTime" value="{{ $meeting->start_datetime }}">
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
    <label for="editEndDateTime">End Date and Time</label>
    <div class="form-group input-group date" id="editenddatetimepicker">
        <input type="text" class="form-control" name="editEndDateTime" value="{{ $meeting->end_datetime }}">
        <span class="input-group-addon">
            <span class="glyphicon glyphicon-calendar"></span>
        </span>
    </div>
    <div class="form-group">
        <button type="submit" class="btn btn-primary">Submit</button>
    </div>
</form>

<form action="/meeting/delete/{{ $meeting->id }}" method="POST">
    <div class="form-group">
        {{ csrf_field() }}
        <input name="_method" type="hidden" value="DELETE">
        <button type="submit" class="btn btn-danger">Delete</button>
    </div>
</form>


@if(session('edit_meeting_error'))
<div class="alert alert-danger" role="alert">
   {{ session('edit_meeting_error') }}
</div>
@endif