{{-- Display a list of all my meetings with edit capabilities --}}
<table class="table my-meetings">
    <thead>
        <tr>
          <th>Room Number</th>
          <th>Start</th>
          <th>End</th>
          <th>Organizer</th>
          <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($meetings as $meeting)
        <tr>
            <th scope="row">{{ $meeting->room_id }}</th>
            <td>{{ $meeting->start_datetime }}</td>
            <td>{{ $meeting->end_datetime }}</td>
            <td>{{ $meeting->organizer_id }}</td>
            <td><a href="/edit/{{ $meeting->id }}">Edit</a></td>
        </tr>
        @endforeach
    </tbody>
</table>