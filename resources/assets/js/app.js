
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');


/*
	Begin our custom functions
*/

jQuery(document).ready(function($)	{
	/*  If datetime field is present, initialize all corresponding datetime pickers  */
	if($("#startdatetimepicker").length) {
		$("#startdatetimepicker").datetimepicker();
		$("#enddatetimepicker").datetimepicker();
	}

	if($("#searchstartdatetimepicker").length) {
		$("#searchstartdatetimepicker").datetimepicker();
		$("#searchenddatetimepicker").datetimepicker();		
	}

	if($("#editstartdatetimepicker").length) {
		$("#editstartdatetimepicker").datetimepicker();
		$("#editenddatetimepicker").datetimepicker();		
	}

	$(".new-meeting-form").submit(function(e)	{
		/*  Init variables  */
		var start = $("input[name=startDateTime]").val();
		var end = $("input[name=endDateTime]").val();
		var currtime = moment().format('MM/DD/YYYY h:mm A');

		/*  Make sure the meeting end time is later than the start time  */
		if(start >= end) {
			alert("Please make sure your meeting end time is later than the start time.");
			e.preventDefault();
			return false;
		}
		/*  Make sure the meeting isn't starting in the past  */
		if(start < currtime) {
			alert("Please make sure your meeting isn't starting in the past!");
			e.preventDefault();
			return false;
		}
	});

	$(".search-meeting-form").submit(function(e)	{
		/*  Init variables  */
		var start = $("input[name=searchStartDateTime]").val();
		var end = $("input[name=searchEndDateTime]").val();

		/*  Make sure the meeting end time is later than the start time  */
		if(start != '' && start >= end) {
			alert("Please make sure your meeting end time is later than the start time.");
			e.preventDefault();
			return false;
		}
		/*  Make sure both start and end dates are set so we can get a valid range  */
		if(start != '' && end == '') {
			alert('Please set an end time.');
			e.preventDefault();
			return false;
		}
		if(start == '' && end != '') {
			alert('Please set a start time.');
			e.preventDefault();
			return false;
		}
	});

	$(".edit-meeting-form").submit(function(e)	{
		/*  Init variables  */
		var start = $("input[name=editStartDateTime]").val();
		var end = $("input[name=editEndDateTime]").val();

		/*  Make sure the meeting end time is later than the start time  */
		if(start != '' && start >= end) {
			alert("Please make sure your meeting end time is later than the start time.");
			e.preventDefault();
			return false;
		}

		/*  Make sure the meeting isn't starting in the past  */
		if(start < currtime) {
			alert("Please make sure your meeting isn't starting in the past!");
			e.preventDefault();
			return false;
		}
		
		/*  Make sure both start and end dates are set so we can get a valid range  */
		if(start != '' && end == '') {
			alert('Please set an end time.');
			e.preventDefault();
			return false;
		}
		if(start == '' && end != '') {
			alert('Please set a start time.');
			e.preventDefault();
			return false;
		}
	});
})

