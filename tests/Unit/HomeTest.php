<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

/*  Use this namespace so we can test while authenticated  */ 
use App\User;

class HomeTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testExample()
    {
        /*  Make sure we can get the homepage */
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)
                         ->get('/')
                         ->assertSee('new-meeting-form')
                         ->assertSee('search-meeting-form')
                         ->assertSee('my-meetings');
    }
}
